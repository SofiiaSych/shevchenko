24.	Не вік дівувати!
25.	Він багатий, одинокий –
26.	Будеш панувати".
27.	"Не хочу я панувати,
28.	Не піду я, мамо!
38.	Тополею стала.
39.	По діброві вітер виє,
40.	Гуляє по полю,
41.	Край дороги гне тополя
42.	До самого долу…
2.	«Тополя»
3.	Полюбила чорнобрива
4.	Козака дівчина.
5.	Полюбила – не спинила:
6.	Пішов – та й загинув...
32.	А дружки заплачуть,
33.	Легше, мамо, в труні лежать,
34.	Ніж його побачить"…
35.	Отак тая чорнобрива
36.	Плакала, співала...
13.	Не стояла б до півночі
14.	З милим під вербою…
15.	Минув і рік, минув другий –
16.	Козака немає;
17.	Сохне вона, як квіточка;